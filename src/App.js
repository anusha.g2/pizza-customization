import React, {useState} from "react";
import Header from "./components/Header";
import {BrowserRouter as Router,Switch,Route} from "react-router-dom"
import Customization from "./components/Customization";
function App() {
    const [ingredients,setIngredients] = useState({
        olive:false,
        basil:false,
        mushroom:false,
        pineapple:false,
        tomato:false,
        cheese:false
    })
    return(
  <div>
      <Header/>
      <Router>
        <Switch>
            <Route exact path="/">
                <Customization ingredients={ingredients} setIngredients={setIngredients}/>
            </Route>
            <Route path="/checkout">
                <h1>Checkout</h1>
            </Route>

        </Switch>
      </Router>
  </div>
    )
}

export default App;
