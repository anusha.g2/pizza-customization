import React from "react";
import BaseCheese from "../assets/BaseCheese.png"
import PizzaBase from "../assets/PizzaBase.png"
import Olive from "../assets/Olive.png"
import Mushroom from "../assets/Mushroom.png"
import Pineapple from "../assets/Pineapple.png"
import Tomato from "../assets/Tomato.png"
import Basil from "../assets/Basil.png"



export default function Customization({ingredients, setIngredients}) {
    const onChange = (event, name) => {
        let newIngredients = JSON.parse(JSON.stringify(ingredients))
        newIngredients[name] = event
        setIngredients(newIngredients);

    }

    return (
        <div style={{display: "flex"}}>
            <div style={{flex: 1, border: "2px solid black"}}>
                <div style={{maxHeight: 500, maxWidth: 500, position: "relative"}}>
                    <img src={BaseCheese} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={Olive} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={Tomato} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={Mushroom} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={Basil} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={Pineapple} alt="Pizza" width="100%" height="100%" className="ingredients"
                         style={{position: "absolute"}}/>
                    <img src={PizzaBase} alt="Pizza" width="100%" height="100%"/>
                </div>
            </div>
            <div style={{flex: "1", border: "2px solid black"}}>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingLeft:"35px",paddingBottom: "10px",position:"relative"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["pineapple"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "pineapple")
                        }}
                    />
                    Pineapple
                </label>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingLeft:"35px",paddingBottom: "10px",position:"relative"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["tomato"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "tomato")
                        }}
                    />
                    Tomato
                </label>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingRight:"5px",paddingBottom: "10px"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["basil"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "basil")
                        }}
                    />
                    Basil
                </label>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingLeft:"35px",paddingBottom: "10px"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["cheese"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "cheese")
                        }}
                    />
                    Cheese
                </label>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingLeft:"35px",paddingBottom: "10px"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["olive"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "olive")
                        }}
                    />
                    Olive
                </label>
                <label class="container-checkbox" style={{display:"block",fontSize:"22px",textAlign:"justify",paddingLeft:"35px",paddingBottom: "10px"}}>

                    <input style={{marginRight: "10px"}}
                        type="checkbox"
                        checked={ingredients["mushroom"]}
                        onChange={(event) => {
                            onChange(event.currentTarget.checked, "mushroom")
                        }}
                    />
                    Mushroom
                </label>
            </div>
        </div>
    )
}